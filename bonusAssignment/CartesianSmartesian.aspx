﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CartesianSmartesian.aspx.cs" Inherits="bonusAssignment.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <h1>Cartesian Smartesian</h1>
            <asp:Label runat="server" ID="lblXAxis" AssociatedControlID="xAxisInput">X-Axis</asp:Label>
            <asp:TextBox runat="server" ID="xAxisInput" placeholder="Enter a Number"></asp:TextBox>
            <asp:RequiredFieldValidator ID="xAxisReqValidator" runat="server" ControlToValidate="xAxisInput" ErrorMessage="Please enter a number!"></asp:RequiredFieldValidator> 
            <asp:CompareValidator ID="xAxisCmpValidator" runat="server" ValueToCompare="0" ControlToValidate="xAxisInput" ErrorMessage="Zero is not allowed!" Operator="NotEqual" Type="Integer"></asp:CompareValidator>
            
            <br/>
            <asp:Label runat="server" ID="lblYAxis" AssociatedControlID="yAxisInput">Y-Axis</asp:Label>
            <asp:TextBox runat="server" ID="yAxisInput" placeholder="Enter a Number"></asp:TextBox>              
            <asp:RequiredFieldValidator ID="yAxisReqValidator" runat="server" ControlToValidate="yAxisInput" ErrorMessage="Please enter a number!"></asp:RequiredFieldValidator>   
            <asp:CompareValidator ID="yAxisCmpValidator" runat="server" ValueToCompare="0" ControlToValidate="yAxisInput" ErrorMessage="Zero is not allowed!" Operator="NotEqual" Type="Integer"></asp:CompareValidator>        
            <br/>
            <asp:Button runat="server" ID="btnSubmit" OnClick="checkCoord" Text="Find Coordinate"></asp:Button>
        </div>
        <div id="result" runat="server"></div>


        <!--/*
            resource for compare validator:
            https://forums.asp.net/t/1884281.aspx?How+to+validate+amount+field+for+not+equal+zero+or+blank+
        */  -->

            
    </form>
</body>
</html>
