﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace bonusAssignment
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void checkCoord(object sender, EventArgs e)
        {
            int xAxis = int.Parse(xAxisInput.Text);
            int yAxis = int.Parse(yAxisInput.Text);

            if (xAxis >= 0 && yAxis >= 0)
            {
                result.InnerHtml = "Quadrant 1";
            }
            else if (xAxis <= 0 && yAxis >= 0)
            {
                result.InnerHtml = "Quadrant 2";
            }
            else if (xAxis <= 0 && yAxis <= 0)
            {
                result.InnerHtml = "Quadrant 3";
            }
            else
            {
                result.InnerHtml = "Quadrant 4";
            }
        }
    }
}