﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace bonusAssignment
{
    public partial class DivisibleSizzible : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void isPrime(object sender, EventArgs e)
        {
            int primeNum = int.Parse(numPrime.Text);
            string loopTester = "";
            string resultStr = "";
            int limit = primeNum - 1;
            int remainder = 0;

            // I actually "hacked" the results for numbers 1 and 2 because when I put it through the forloop logic 
            // number 1 will not print any result and number 2 will not be a prime number
            if (primeNum == 1)
            {
                resultStr = primeNum + " is not a Prime Number";
            }
            else if (primeNum == 2)
            {
                resultStr = primeNum + " is a Prime Number";
            }
            else
            {
                for (int ctr = limit; ctr > 1; ctr--)
                {
                    remainder = primeNum % ctr;
                    //for testing purposes
                    //loopTester += primeNum + "%" + ctr + " = " + remainder + "<br/>"; 

                    if (primeNum % ctr == 0)
                    {
                        resultStr = primeNum + " is not a Prime Number";
                        break; //exit loop if remainder hits 0
                    }
                    else
                    {
                        resultStr = primeNum + " is a Prime Number";
                    }
                }
            }
            primeResult.InnerHtml = resultStr;
        }
    }
}