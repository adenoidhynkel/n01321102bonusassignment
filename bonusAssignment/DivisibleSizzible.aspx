﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DivisibleSizzible.aspx.cs" Inherits="bonusAssignment.DivisibleSizzible" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <h1>Divisible Sizzible</h1>
            <asp:Label runat="server" ID="lblPrime" AssociatedControlID="numPrime">Enter Prime Number</asp:Label>
            <asp:TextBox runat="server" ID="numPrime" placeholder="Enter a number"></asp:TextBox>
            <asp:RequiredFieldValidator ID="numPrimeReqValidator" runat="server" ControlToValidate="numPrime" ErrorMessage="Please enter a number!"></asp:RequiredFieldValidator> 
            <asp:CompareValidator ID="numPrimeCmpValidator" runat="server" ValueToCompare="0" ControlToValidate="numPrime" ErrorMessage="Zero is not allowed!" Operator="NotEqual" Type="Integer"></asp:CompareValidator>
            <br/>
            <asp:Button runat="server" ID="checkPrime" Text="Check Number" OnClick="isPrime"></asp:Button>
        </div>
        <div id="primeResult" runat="server"></div>
          
        <!--/*
            resource for compare validator:
            https://forums.asp.net/t/1884281.aspx?How+to+validate+amount+field+for+not+equal+zero+or+blank+
        */  -->
    </form>
</body>
</html>
