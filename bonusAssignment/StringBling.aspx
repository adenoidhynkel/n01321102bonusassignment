﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="StringBling.aspx.cs" Inherits="bonusAssignment.StringBling" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <h1>String Bling</h1>
            <asp:Label runat="server" ID="lblPalindrome" AssociatedControlID="palindromeStr">Enter a Word</asp:Label>
            <asp:TextBox runat="server" ID="palindromeStr" placeholder="Enter a Word"></asp:TextBox>
            <asp:RequiredFieldValidator ID="palindromeReqValidator" runat="server" ControlToValidate="palindromeStr" ErrorMessage="Please enter a word"></asp:RequiredFieldValidator>
            <%/*
               the regex you provided triggers when there is a space in the string like race car so I searched for a regex that allows it and will just filter the string on the code behind using the replace function.
               The answer below states that I only have to add space on the regex code you provided :)

               https://stackoverflow.com/questions/15472764/regular-expression-to-allow-spaces-between-words
               reference for the validator
               https://www.javatpoint.com/asp-net-web-form-regular-expression-validator
            
             */ %>
            <asp:RegularExpressionValidator ID="wordValidator" runat="server" ControlToValidate="palindromeStr" ErrorMessage="Please enter valid string" ValidationExpression="^[a-zA-Z ]+$"> </asp:RegularExpressionValidator>
            <br/>
            <asp:Button runat="server" ID="checkPalindrome" Text="Check Word" OnClick="isPalindrome"></asp:Button>
        </div>
        <div id="palindromeResult" runat="server"></div>
    </form>
</body>
</html>
