﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace bonusAssignment
{
    public partial class StringBling : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void isPalindrome(object sender, EventArgs e)
        {
            /*  
             *  resource for removing whitespace https://stackoverflow.com/questions/3905180/how-to-trim-whitespace-between-characters
                tochararray https://www.dotnetperls.com/tochararray 
            */
            string strPalindrome = palindromeStr.Text;
            strPalindrome = strPalindrome.ToLower();
            strPalindrome = strPalindrome.Replace(" ", "");
            char [] charPalindrome = strPalindrome.ToCharArray();
            Array.Reverse(charPalindrome);
            Boolean checkPalindrome = false;

            for (int ctr = 0; ctr < strPalindrome.Length; ctr++)
            {
                if (strPalindrome[ctr].Equals(charPalindrome[ctr]) == true)
                {
                    checkPalindrome = true;
                }
                else
                {
                    checkPalindrome = false;
                    // exit the loop if the strPalindrome[ctr] is not equal to charPalindrome[ctr]
                    break; 
                }
            }

            if (checkPalindrome == true)
            {
                palindromeResult.InnerHtml = "<br/>"+ strPalindrome + " is a palindrome";
            }
            else
            {
                palindromeResult.InnerHtml = "<br/>" + strPalindrome + " is not a palindrome";
            }
        }
    }
}